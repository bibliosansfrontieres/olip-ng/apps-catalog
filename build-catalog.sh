#!/bin/bash
VISIBILITY=public
GITLAB_API=https://gitlab.com/api/v4

if [ -z "$BRANCH" ]; then
  BRANCH=main
fi

fetch_git() {
  curl -s --header "Authorization: Bearer $APPLICATIONS_ACCESS_TOKEN" "$*"
}

say() {
  echo >&2 "[+] $*"
}

check_file_exists() {
  result_status=$(curl --header "Authorization: Bearer $APPLICATIONS_ACCESS_TOKEN" -s -o /dev/null -w "%{http_code}" "${APP_FILES}/$1/raw?ref=$BRANCH")
  if [ "$result_status" != 200 ]; then
    if [ -n "$2" ] && [ "$2" = "true" ]; then
      say "$1 not found for project $id $path, skipping..."
    fi
    return 1
  fi
  return 0
}

get_file() {
  say "#$id:$path - Getting $1"
  curl -s --header "Authorization: Bearer $APPLICATIONS_ACCESS_TOKEN" "${APP_FILES}/$1/raw?ref=$BRANCH" > "output/$id-$1"
}

mkdir -p output
say "Fetching $VISIBILITY/$BRANCH apps repositories..."
projects=$(fetch_git "$GITLAB_API/groups/85835811/projects?visibility=$VISIBILITY")
projects_list=$(echo "$projects" | jq -r '.[] | "\(.id) \(.path_with_namespace)"')
projects_count=$(echo "$projects" | jq '. | length')
say "Found $projects_count projects !"

echo "$projects_list" | while read -r id path; do
say "#$id:$path - Processing..."
APP_FILES="$GITLAB_API/projects/$id/repository/files"

if ! check_file_exists "description.json" true; then
  continue;
fi

if ! check_file_exists "hooks.json" true; then
  continue;
fi

if ! check_file_exists "compose.yml" true; then
  continue;
fi

get_file "description.json"
get_file "hooks.json"
get_file "compose.yml"

if check_file_exists "catalog.js"; then
  get_file "catalog.js"
  catalog_content=$(< "output/$id-catalog.js")
fi

compose_content=$(< "output/$id-compose.yml")
hooks_content=$(< "output/$id-hooks.json")

if [ -n "$catalog_content" ]; then
  jq --arg catalogFunction "$catalog_content" '.catalogFunction = $catalogFunction' "output/$id-description.json" > "output/$id-description.tmp"
  mv "output/$id-description.tmp" "output/$id-description.json"
fi

# Set compose property
jq --arg compose "$compose_content" '.compose = $compose' "output/$id-description.json" > "output/$id-description.tmp"
mv "output/$id-description.tmp" "output/$id-description.json"

# Set hooks property
jq --argjson hooks "$hooks_content" '.hooks = $hooks' "output/$id-description.json" > "output/$id-description.tmp"
  mv "output/$id-description.tmp" "output/$id-description.json"
  say "#$id:$path - Processed !"
done

say "All apps repositories processed !"
say "Generating catalog.json..."

cat output/*-description.json | jq -s '{applications: .}' > output/catalog.json

say "Done !"